import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas

Router.menuElement = document.querySelector('.mainMenu');

const carte = document.querySelector('.newsContainer');
carte.setAttribute( 'style', 'display:' );

function onClick( event ) {
	event.preventDefault(); // empêche le rechargement de la page
	carte.setAttribute( 'style', 'display:none');
}
const button = document.querySelector('.closeButton');
button.addEventListener('click', onClick);